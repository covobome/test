<?php

namespace App;

use App\Figure\Circle;
use App\Figure\Contract\FigureContract;
use App\Figure\Contract\FigureFactoryContract;
use App\Figure\Square;
use App\Figure\Support\Collection;
use App\Scene\Contract\SceneContract;

/**
 * Класс Application.
 * Точка входа в приложение.
 */
class Application
{
    /**
     * @var FigureFactoryContract
     */
    protected $figureFactory;

    /**
     * @var FigureFactoryContract
     */
    protected $scene;

    /**
     * @var Collection|FigureContract[]
     */
    protected $collection;

    /**
     * @param SceneContract $scene
     * @param FigureFactoryContract $figureFactory
     */
    public function __construct(SceneContract $scene, FigureFactoryContract $figureFactory)
    {
        $this->scene         = $scene;
        $this->figureFactory = $figureFactory;
        $this->collection    = new Collection();
    }

    /**
     * Запустить приложение.
     */
    public function bootstrap()
    {
        $this->bindFigures();
    }

    /**
     * Установить псевдонимы для фигур.
     */
    protected function bindFigures()
    {
        $this->figureFactory->bind('circle', Circle::class);
        $this->figureFactory->bind('square', Square::class);
    }

    /**
     * Получить построитель фигур.
     *
     * @return FigureFactoryContract
     */
    public function getFigureFactory()
    {
        return $this->figureFactory;
    }

    /**
     * Добавить фигуры в приложение.
     *
     * @param array $shapes
     */
    public function addFigures(array $shapes)
    {
        foreach ($shapes as $shapeParams) {
            $this->collection->push(
                $this->figureFactory->factory($shapeParams['type'], $shapeParams['params'])
            );
        }
    }

    /**
     * Нарисовать все фигуры.
     */
    public function draw()
    {
        $this->scene->flush();
        $this->collection->each(function (FigureContract $figure) {
            $figure->drawTo($this->scene);
        });
        $this->scene->draw();
    }
}