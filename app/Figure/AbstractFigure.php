<?php

namespace App\Figure;

use App\Figure\Contract\FigureContract;
use App\Figure\Exception\UndefinedFigureProperty;

abstract class AbstractFigure implements FigureContract
{
    protected $params = [
        'x' => 1,
        'y' => 1,
    ];

    /**
     * Circle constructor.
     * @param array $params
     * @throws UndefinedFigureProperty
     */
    public function __construct($params)
    {
        foreach ($params as $paramName => $value) {
            if (!isset($this->params[$paramName])) {
                throw new UndefinedFigureProperty($paramName);
            }

            $this->params[$paramName] = $value;
        }
    }
}