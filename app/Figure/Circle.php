<?php

namespace App\Figure;

use App\Scene\Contract\SceneContract;

/**
 * Круг.
 * @package App\Figure
 */
class Circle extends AbstractFigure
{
    protected $params = [
        'x'      => 1,
        'y'      => 1,
        'radius' => 1,
    ];

    /**
     * @inheritdoc
     */
    public function drawTo(SceneContract $scene)
    {
        // https://ru.wikipedia.org/wiki/%D0%90%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC_%D0%91%D1%80%D0%B5%D0%B7%D0%B5%D0%BD%D1%85%D1%8D%D0%BC%D0%B0
        $x = 0;
        $X1 = $this->params['x'];
        $Y1 = $this->params['y'];
        $y = $this->params['radius'];
        $delta = 1 - 2 * $this->params['radius'];

        while ($y >= 0) {
            $scene->fillPoint($X1 + $x, $Y1 + $y, '@');
            $scene->fillPoint($X1 + $x, $Y1 - $y, '@');
            $scene->fillPoint($X1 - $x, $Y1 + $y, '@');
            $scene->fillPoint($X1 - $x, $Y1 - $y, '@');
            $error = 2 * ($delta + $y) - 1;
            if (($delta < 0) && ($error <= 0)) {
                $delta += 2 * ++$x + 1;
                continue;
            }
            $error = 2 * ($delta - $x) - 1;
            if (($delta > 0) && ($error > 0)) {
                $delta += 1 - 2 * --$y;
                continue;
            }
            $x++;
            $delta += 2 * ($x - $y);
            $y--;
        }
    }
}
