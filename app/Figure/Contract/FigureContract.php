<?php

namespace App\Figure\Contract;

use App\Scene\Contract\SceneContract;

interface FigureContract
{
    /**
     * Нарисовать фигуру на сцене.
     *
     * @param SceneContract $scene
     * @return mixed
     */
    public function drawTo(SceneContract $scene);
}