<?php

namespace App\Figure\Contract;

use App\Figure\Exception\UndefinedFigure;

interface FigureFactoryContract
{
    /**
     * Установить соответствие псевдонима и фигуры.
     *
     * @param $alias
     * @param $className
     */
    public function bind($alias, $className);

    /**
     * Получить фигуру с заданными параметрами
     *
     * @param $alias
     * @param $params
     * @throws UndefinedFigure
     * @return FigureContract
     */
    public function factory($alias, $params) : FigureContract;
}