<?php

namespace App\Figure\Exception;

/**
 * Class UndefinedFigure.
 * Неизсветная фигура.
 *
 * @package App\Figure\Exception
 */
class UndefinedFigure extends \Error
{

}