<?php

namespace App\Figure\Exception;

/**
 * Class UndefinedFigureProperty.
 * Неизсветное свойство фигуры.
 *
 * @package App\Figure\Exception
 */
class UndefinedFigureProperty extends \Error
{

}