<?php

namespace App\Figure;

use App\Figure\Contract\FigureContract;
use App\Figure\Contract\FigureFactoryContract;
use App\Figure\Exception\UndefinedFigure;

/**
 * Построитель фигур по параметрам.
 *
 * @package App\Figure
 */
class FigureFactory implements FigureFactoryContract
{
    /**
     * @var array
     */
    protected $binds = [];

    /**
     * @inheritdoc
     */
    public function bind($alias, $className)
    {
        $this->binds[$alias] = $className;
    }

    /**
     * @inheritdoc
     */
    public function factory($alias, $params) : FigureContract
    {
        if (! isset($this->binds[$alias])) {
            throw new UndefinedFigure($alias);
        }

        $className = $this->binds[$alias];
        return new $className($params);
    }
}
