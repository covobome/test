<?php

namespace App\Figure;

use App\Scene\Contract\SceneContract;

/**
 * Квадрат.
 * @package App\Figure
 */
class Square extends AbstractFigure
{
    protected $params = [
        'x'      => 1,
        'y'      => 1,
        'length' => 1,
    ];

    /**
     * @inheritdoc
     */
    public function drawTo(SceneContract $scene)
    {
        // верхняя и нижняя грань
        for ($startX = $this->params['x']; $startX <= $this->params['x'] + $this->params['length']; $startX++) {
            $scene->fillPoint($startX, $this->params['y'], '-');
            $scene->fillPoint($startX, $this->params['y'] + $this->params['length'], '-');
        }

        // правая и левая грань
        for ($startY = $this->params['y']; $startY <= $this->params['y'] + $this->params['length']; $startY++) {
            $scene->fillPoint($this->params['x'], $startY, '|');
            $scene->fillPoint($this->params['x'] + $this->params['length'], $startY, '|');
        }
    }
}
