<?php

namespace App\Figure\Support;

use App\Figure\Contract\FigureContract;
use ArrayAccess;
use Countable;
use IteratorAggregate;

/**
 * Класс Collection.
 * Представляет собой удобный агрегатор фигур.
 *
 * @package App\Figure\Support
 */
class Collection implements ArrayAccess, Countable, IteratorAggregate
{
    /**
     * @var array
     */
    protected $items = [];

    /***
     * @param  mixed  $items
     */
    public function __construct($items = [])
    {
        $this->items = $items;
    }

    /**
     * Выполнить колбэк для каждой фигуры.
     *
     * @param  callable  $callback
     * @return $this
     */
    public function each(callable $callback)
    {
        foreach ($this->items as $item) {
            if ($callback($item) === false) {
                break;
            }
        }

        return $this;
    }

    /**
     * Положить фигуру в коллекцию.
     *
     * @param  mixed  $value
     * @return $this
     */
    public function push(FigureContract $value)
    {
        $this->offsetSet(null, $value);

        return $this;
    }

    /**
     * Получить итератор по фигурам.
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    /**
     * Получить количество фигур.
     *
     * @return int
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     * Проверить на существование фигуры по индексу.
     *
     * @param  mixed  $key
     * @return bool
     */
    public function offsetExists($key)
    {
        return array_key_exists($key, $this->items);
    }

    /**
     * Получить фигуру по индексу.
     *
     * @param  mixed  $key
     * @return mixed
     */
    public function offsetGet($key)
    {
        return $this->items[$key];
    }

    /**
     * Установить фигуру в коллекцию по индексу.
     *
     * @param  mixed  $key
     * @param  mixed  $value
     * @return void
     */
    public function offsetSet($key, $value)
    {
        if (is_null($key)) {
            $this->items[] = $value;
        } else {
            $this->items[$key] = $value;
        }
    }

    /**
     * Удалить фигуру из коллекцию по ее индексу.
     *
     * @param  string  $key
     * @return void
     */
    public function offsetUnset($key)
    {
        unset($this->items[$key]);
    }
}