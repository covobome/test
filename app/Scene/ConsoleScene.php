<?php

namespace App\Scene;

use App\Scene\Contract\SceneContract;

class ConsoleScene implements SceneContract
{
    protected $points = [];

    /**
     * @inheritdoc
     */
    public function flush()
    {
        $this->points = [];
    }

    /**
     * @inheritdoc
     */
    public function fillPoint($x, $y, $symbol)
    {
        if (empty($this->points[$x])) {
            $this->points[$x] = [];
        }

        $this->points[$x][$y] = $symbol;
    }

    /**
     * @inheritdoc
     */
    public function draw()
    {
        $boardHeight = max(array_keys($this->points));
        $boardLength = 0;
        foreach ($this->points as $horizontal) {
            $boardLength = max($boardLength, max(array_keys($horizontal)));
        }

        for ($y = 0; $y <= $boardHeight; $y++) {
            for ($x = 0; $x <= $boardLength; $x++) {
                if (empty($this->points[$x][$y])) {
                    echo ' ';
                } else {
                    echo $this->points[$x][$y];
                }
            }
            echo "\n";
        }
    }
}