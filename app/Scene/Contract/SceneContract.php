<?php

namespace App\Scene\Contract;

interface SceneContract
{
    /**
     * Очистить сцену.
     */
    public function flush();

    /**
     * Заполнить точку (пиксель) сцены.
     *
     * @param int $x
     * @param int $y
     * @param string $symbol
     */
    public function fillPoint($x, $y, $symbol);

    /**
     * Нарисовать сцену.
     */
    public function draw();
}