<?php

require __DIR__ . '/../vendor/autoload.php';

use App\Application;
use App\Figure\FigureFactory;
use App\Scene\ConsoleScene;


/**
 * Точка входа в приложение.
 *
 * @param array $shapes
 */
function main($shapes)
{
    $scene = new ConsoleScene();
    $factory = new FigureFactory();

    // ты можешь добавить свои фигуры
    // $factory->bind('figureName', 'className');

    $app = new Application($scene, $factory);
    $app->bootstrap();
    $app->addFigures($shapes);
    $app->draw();

    sleep(2);

    echo "\n\nДинамически добавили круг:\n";
    $app->addFigures([
        [
            'type'   => 'circle',
            'params' => [
                'x'      => '5',
                'y'      => '5',
                'radius' => '2',
            ],
        ]
    ]);
    $app->draw();

    sleep(2);

    echo "\n\nДинамически добавили квадрат:\n";
    $app->addFigures([
        [
            'type'   => 'square',
            'params' => [
                'x'      => '10',
                'y'      => '10',
                'length' => '4',
            ],
        ]
    ]);
    $app->draw();
    exit;
}

$initialFigures = [
    [
        'type'   => 'circle',
        'params' => [
            'x'      => '6',
            'y'      => '6',
            'radius' => '5',
        ],
    ],
    [
        'type'   => 'square',
        'params' => [
            'x'      => '10',
            'y'      => '10',
            'length' => '8',
        ],
    ],
];

main($initialFigures);